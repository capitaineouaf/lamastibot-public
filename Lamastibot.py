"""
Lamastibot — Main

SAKIUT Custom License

Copyright (c) 2018-present Sakiut

Permission is hereby granted, free of charge, to any person to obtain a copy
of this software and associated documentation files (the “Software”). Subject
to the foregoing sentence, you are free to modify this Software and publish
patches to the Software. You agree that Sakiut retain all right, title and interest
in and to all such modifications and/or patches, and all such modifications and/or
patches may only be used, copied, modified, displayed, distributed, or otherwise
exploited with the authorization of the author. Notwithstanding the foregoing,
you may not copy and modify the Software for development and testing purposes,
without requiring a subscription. You agree that Sakiut retain all right,
title and interest in and to all such modifications. You are not granted any
other rights beyond what is expressly stated herein. Subject to the
foregoing, it is forbidden to copy, merge, publish, distribute, sublicense,
and/or sell the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

For all third party components incorporated into the Software, those
components are licensed under the original license provided by the owner of the
applicable component.

contact@sakiut.fr
"""

from datetime import datetime, timedelta, date
import asyncio
import random
import traceback
import sys

import discord
import discord.errors
from discord.ext import commands

from libs import lib, twitch, db, fourasticot, captcha
from libs import blacklist
from libs.xp_levels import *

print(f'[L][{datetime.now().strftime("%x %X")}] Connecting...')


class Bot(commands.AutoShardedBot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, activity=discord.Game(name="Corobizar.com | !help"), **kwargs)

        # General variables

        self.owner_id = 187565415512276993
        self.launch_time = datetime.now()
        self.date = date.today()
        self.blocked_list = db.get_xp_blocked()
        self.xp_boost = db.get_boost()["boost"]

        # Debug

        self.debug = False
        self.debug_channel = self.get_channel(595653106562498560)

        # Logs

        self.logs_channel = self.get_channel(243743270939787265)

        # Cogs

        from cogs.settings import Settings, Issues
        from cogs.profile import Profile
        from cogs.moderation import Moderation
        from cogs.events import Events

        self.add_cog(Moderation(self))
        self.add_cog(Profile(self))
        self.add_cog(Settings(self))
        self.add_cog(Events(self))
        self.add_cog(Issues(self))

        # Stream detection

        self.stream_id = None

        # Fourasticot

        self.fourasticot_channels = lib.get_everyone_channels()
        self.next_fourasticot = datetime.utcnow() + timedelta(minutes=random.randint(59, 150))

        # Other

        self.blacklist = blacklist.get()

    async def on_ready(self):

        await self.wait_until_ready()

        print('-------------------------------------------------------------')
        print(f'[L][{datetime.now().strftime("%x %X")}] Logged in as')
        print(f'[L][{datetime.now().strftime("%x %X")}]', self.user.name)
        print(f'[L][{datetime.now().strftime("%x %X")}]', self.user.id)
        print('-------------------------------------------------------------')

        # Loops' run

        while True:

            # Getting important channels

            self.debug_channel = self.get_channel(595653106562498560)
            self.logs_channel = self.get_channel(243743270939787265)

            # Launching looped tasks

            await self.check_temp(self.get_guild(220309354266624000))
            await self.stream_start_check(self.get_channel(220585726423728130))
            await self.fourasticot_check()
            await self.coin_check()
            await asyncio.sleep(30)

    async def on_member_ban(self, guild, member):

        act = discord.AuditLogAction.ban

        def predicate(event):
            return event.target == member and event.action == act

        entry = await guild.audit_logs().find(predicate)

        debug_embed = await db.create_entry(
            bot=self,
            user=member,
            sanction="Ban",
            channel="N/A",
            author=entry.user,
            date=entry.created_at,
            reason=entry.reason,
            comment="Aucun"
        )

        if self.debug:
            await self.debug_channel.send(embed=debug_embed)

        log_ban_embed = discord.Embed()
        log_ban_embed.colour = 0xFF0000
        log_ban_embed.title = "Un utilisateur a été banni du serveur"
        log_ban_embed.set_thumbnail(url=member.avatar_url)
        log_ban_embed.add_field(name="Utilisateur banni", value=member, inline=False)
        log_ban_embed.add_field(name="_ _", value="_ _", inline=False)
        log_ban_embed.add_field(name="Auteur", value=entry.user, inline=True)
        log_ban_embed.add_field(name="Date", value=entry.created_at.strftime("%x %X"), inline=True)
        log_ban_embed.add_field(name="_ _", value="_ _", inline=False)
        log_ban_embed.add_field(name="Raison", value=entry.reason)
        log_ban_embed.set_footer(text=f"{datetime.now().strftime('%x %X')} | UID : {member.id}")

        await self.logs_channel.send(embed=log_ban_embed)

    async def on_member_join(self, member):
        if member.bot:
            return
        profile = await db.get_profile(member)
        if profile is None:
            await db.create_profile(member)

        log_join_embed = discord.Embed()
        log_join_embed.colour = 0x00FF00
        log_join_embed.title = "Un utilisateur a rejoint le serveur"
        log_join_embed.set_thumbnail(url=member.avatar_url)
        log_join_embed.add_field(name="Utilisateur", value=member, inline=False)
        log_join_embed.add_field(name="_ _", value="_ _", inline=False)
        log_join_embed.add_field(name="Date", value=datetime.now().strftime("%x %X"), inline=True)
        log_join_embed.set_footer(text=f"UID : {member.id}")

        await self.logs_channel.send(embed=log_join_embed)

    async def on_member_remove(self, member):

        log_left_embed = discord.Embed()
        log_left_embed.colour = 0xFF0000
        log_left_embed.title = "Un utilisateur a quitté le serveur"
        log_left_embed.set_thumbnail(url=member.avatar_url)
        log_left_embed.add_field(name="Utilisateur", value=member, inline=False)
        log_left_embed.add_field(name="_ _", value="_ _", inline=False)
        log_left_embed.add_field(name="Date", value=datetime.now().strftime("%x %X"), inline=True)
        log_left_embed.add_field(name="_ _", value="_ _", inline=False)
        log_left_embed.set_footer(text=f"UID : {member.id}")

        await self.logs_channel.send(embed=log_left_embed)

    async def on_message_edit(self, before, after):

        if before.author.bot:
            return

        if before.content.startswith("http"):
            return

        if len(before.content) >= 1750:
            content = before.content[:1750]
            content += "..."
        else:
            content = before.content

        log_msg_edit_embed = discord.Embed()
        log_msg_edit_embed.colour = 0x0DC5AD
        log_msg_edit_embed.title = "Un utilisateur a modifié son message"
        log_msg_edit_embed.set_thumbnail(url=before.author.avatar_url)
        log_msg_edit_embed.add_field(name="Utilisateur", value=before.author.mention, inline=False)
        log_msg_edit_embed.add_field(name="_ _", value="_ _", inline=False)
        log_msg_edit_embed.add_field(name="Date", value=datetime.now().strftime("%x %X"), inline=True)
        log_msg_edit_embed.add_field(name="Channel", value=before.channel.mention, inline=True)
        log_msg_edit_embed.add_field(name="_ _", value="_ _", inline=False)
        log_msg_edit_embed.add_field(name="Contenu de l'ancien message", value=f"```\n{content}\n```", inline=False)
        log_msg_edit_embed.add_field(name="_ _", value="_ _", inline=False)
        log_msg_edit_embed.set_footer(
            text=f"Msg datetime : {before.created_at.strftime('%x %X')} | UID : {before.author.id}"
        )

        try:
            await self.logs_channel.send(embed=log_msg_edit_embed)
        except discord.errors.HTTPException:
            return

    async def on_message_delete(self, message):

        if message.author.bot:
            return

        if len(message.content) >= 1750:
            content = message.content[:1750]
            content += "..."
        else:
            content = message.content

        log_msg_del_embed = discord.Embed()
        log_msg_del_embed.colour = 0xE67E22
        log_msg_del_embed.title = "Un message a été supprimé"
        log_msg_del_embed.set_thumbnail(url=message.author.avatar_url)
        log_msg_del_embed.add_field(name="Auteur", value=message.author.mention, inline=False)
        log_msg_del_embed.add_field(name="_ _", value="_ _", inline=False)
        log_msg_del_embed.add_field(name="Date", value=datetime.now().strftime("%x %X"), inline=True)
        try:
            log_msg_del_embed.add_field(name="Channel", value=message.channel.mention, inline=True)
        except AttributeError:
            print("Error while trying to log deleted message:")
            print(f"\t{type(message.channel)} - User: {message.channel.recipient} ({message.channel.recipient.id})")
            return
        log_msg_del_embed.add_field(name="_ _", value="_ _", inline=False)
        log_msg_del_embed.add_field(name="Contenu du message", value=f"```\n{content}\n```", inline=False)
        log_msg_del_embed.add_field(name="_ _", value="_ _", inline=False)
        log_msg_del_embed.set_footer(
            text=f"Msg datetime : {message.created_at.strftime('%x %X')} | UID : {message.author.id}"
        )

        try:
            await self.logs_channel.send(embed=log_msg_del_embed)
        except discord.errors.HTTPException:
            return

    async def on_raw_reaction_add(self, payload):

        if not payload.message_id == lib.get_roles_message_id():
            return

        guild = self.get_guild(220309354266624000)
        user = guild.get_member(payload.user_id)

        if payload.emoji.id == 468820667182088202:
            await user.add_roles(lib.get_role(guild, 709158421345337344))
            try:
                await user.send("Le rôle *Viewer* vous a été ajouté !")
            except discord.Forbidden:
                pass

    async def on_raw_reaction_remove(self, payload):

        if not payload.message_id == lib.get_roles_message_id():
            return

        guild = self.get_guild(220309354266624000)
        user = guild.get_member(payload.user_id)

        if payload.emoji.id == 468820667182088202:
            await user.remove_roles(lib.get_role(guild, 709158421345337344))
            try:
                await user.send("Le rôle *Viewer* vous a été retiré !")
            except discord.Forbidden:
                pass

    async def on_message(self, message):

        ctx = await self.get_context(message)

        if self.debug:
            self.debug_channel = message.guild.get_channel(595653106562498560)

        if message.author.bot:
            return

        if message.guild is None:
            await self.process_commands(message)
            return

        ad_blacklist = [
            "clash",
            "discord.gg",
        ]

        if message.channel.id == 285429208761761793:
            for word in ad_blacklist:
                if word in message.content.lower():
                    await message.delete()
                    msg = "Bonjour ! Merci de lire les épinglés avant de poster une annonce !"
                    try:
                        await message.author.send(msg)
                    except discord.Forbidden:
                        await message.channel.send(f"{message.author.mention} " + msg, delete_after=600)
                    await self.logs_channel.send(f"{message.author.mention} a utilisé le banword {word}.")

        # if message.author.top_role.id in (251419879951958027, 220310157513457664):

        levels = get_levels()
        level_roles = get_level_roles()

        profile = await db.get_profile(message.author)
        if profile is None:
            profile = await db.create_profile(message.author)

        if message.author.id in self.blacklist:
            blacklist.save(message.author, message)

        try:
            last_message_date = profile['last_message_date'].split("+")[0]
            last_flake = profile['last_flake'].split("+")[0]
        except TypeError:
            await self.process_commands(message)
            return

        if last_flake is None:
            last_flake = last_message_date

        fmt = "%Y-%m-%dT%H:%M:%S.%f"
        try:
            last_message_date = datetime.strptime(last_message_date, fmt)
        except ValueError:
            fmt = "%Y-%m-%dT%H:%M:%S"
            last_message_date = datetime.strptime(last_message_date, fmt)
        lib.crop_microseconds(last_message_date)

        fmt = "%Y-%m-%dT%H:%M:%S.%f"
        try:
            last_flake = datetime.strptime(last_flake, fmt)
        except ValueError:
            fmt = "%Y-%m-%dT%H:%M:%S"
            last_flake = datetime.strptime(last_flake, fmt)
        lib.crop_microseconds(last_flake)

        if message.content[1:] == message.content[:-1]:
            await self.process_commands(message)
            return

        if message.content.lower().startswith("https://") or message.content.lower().startswith("http://"):
            await self.process_commands(message)
            return

        if message.content.lower().startswith("<:") and message.content.lower().endswith(">") \
                and message.content.lower().count(":") == 2:
            await self.process_commands(message)
            return

        if message.author.id in self.blocked_list:
            await self.process_commands(message)
            return

        # christmas_role = lib.get_role(ctx.guild, 518427790417199113)
        #
        # if last_flake.day < message.created_at.day:
        #     profile["today_flakes"] = 50
        #     profile["flakes_2019"] += 50
        #     profile["msgs_before_last_flake"] = 0
        #     profile["last_flake"] = message.created_at
        #     if not (christmas_role in message.author.roles):
        #         try:
        #             await message.author.send("En vous réveillant ce matin, vous récoltez 50 flocons !")
        #         except discord.errors.Forbidden:
        #             pass
        #
        # elif (message.created_at - last_flake) > timedelta(minutes=random.randint(8, 12)) \
        #         and len(message.content) >= 3 \
        #         and profile["msgs_before_last_flake"] >= random.randint(10, 14):
        #
        #     if profile["today_flakes"] < 200:
        #         profile["today_flakes"] += 10
        #         profile["flakes_2019"] += 10
        #         profile["msgs_before_last_flake"] = 0
        #         if profile["today_flakes"] == 200:
        #             await message.author.send("Bravo, vous avez collecté le maximum de flocons disponibles "
        #                                       "aujourd'hui !")
        #
        #     profile["last_flake"] = message.created_at
        #
        # else:
        #     profile["msgs_before_last_flake"] += 1
        #
        # if profile["flakes_2019"] >= 1000:
        #     profile["flakes_2019"] = 1000
        #
        #     if not (christmas_role in message.author.roles):
        #         await message.author.add_roles(christmas_role)
        #         await message.author.send("Bravo à toi jeune lama, tu as obtenu le rôle *Ramasseur de flocons* pour "
        #                                   "avoir été actif ce mois de décembre ! Joyeux Noël !")

        debug_embed = None

        if self.debug:
            debug_embed = discord.Embed()
            debug_embed.title = f"Starting XP attribution for {message.author}"
            debug_embed.description = "```\n"

        if last_message_date.day < message.created_at.day:

            try:
                amount = levels[profile['level'] + 1] - levels[profile['level']]
            except IndexError:
                amount = 0

            profile['xp_points'] += int(0.02 * amount * self.xp_boost)
            profile['last_message_date'] = message.created_at

            if self.debug:
                debug_embed.description += f"FMotD Bonus : {str(int(0.02 * amount * self.xp_boost))}\n"

        elif (message.created_at - last_message_date) > timedelta(minutes=random.randint(4, 8)) \
                and len(message.content) >= 3 \
                and profile["msgs_before_last_xp"] >= random.randint(1, 2):

            profile['xp_points'] += 15 * self.xp_boost * 2
            profile["msgs_before_last_xp"] = 0

            if self.debug:
                debug_embed.description += f"Message XP give : {15 * self.xp_boost}\n"

            profile['last_message_date'] = message.created_at

        else:
            profile["msgs_before_last_xp"] += 1
            if self.debug:
                debug_embed.description += "No XP this time\n"

        if profile['xp_points'] > 225000:
            profile['xp_points'] = 225000
            if self.debug:
                debug_embed.description += "XP Already at maximum → Back to 225000\n"

        if self.debug:
            debug_embed.add_field(name="Profile final XP", value=profile["xp_points"])
            debug_embed.add_field(name="Final messages nb", value=profile["msgs_before_last_xp"])

        if profile['xp_points'] == 15:
            level = 1
        else:
            i = 0

            for level in levels:
                if level > profile['xp_points']:
                    break
                i += 1

            level = i - 1

        if self.debug:
            debug_embed.add_field(name="Final Level", value=str(level))

        if level == profile['level']:
            pass

        else:
            # alpha_channel = self.get_channel(478037718987964427)
            profile['level'] = level
            try:
                role = list(filter(lambda x: x.id == level_roles[level], message.guild.roles))
            except AttributeError:
                await self.process_commands(message)
                return
            role = role[0]

            await lib.delete_level_roles(message.author)
            await message.author.add_roles(role)

            colors = {
                20: "grey",
                30: "turquoise",
                40: "candy",
                50: "red",
            }

            try:
                if level in (20, 30, 40):
                    await message.channel.send(f"<a:PORO:486496641541603329> {message.author.mention} est désormais "
                                               f"niveau **{level}** ! GG  <a:PORO:486496641541603329> !")
                    image = discord.File(f"./data/colors_source/{colors[level]}.png")
                    await message.author.send(f"Vous avez atteint le niveau **{level}**, vous avez donc débloqué une "
                                              f"nouvelle couleur : {colors[level].capitalize()}", file=image)
                elif level == 50:
                    profile["max_level_date"] = datetime.today()
                    soken = self.get_user(121928946320146432)
                    await message.channel.send(
                        f"<a:LAMACOIN:485323716390158367> Que tout le monde acclame {message.author.mention} pour avoir"
                        f" atteint l'ultime niveau 50, Félicitations ! <a:LAMACOIN:485323716390158367>"
                    )
                    await soken.send(f"{message.author.mention}({message.author.id}) a atteint le niveau 50"
                                     f" !")

                    image = discord.File(f"./data/colors_source/{colors[level]}.png")
                    await message.author.send(f"Vous avez atteint le niveau **{level}**, vous avez donc débloqué une"
                                              f"nouvelle couleur : {colors[level].capitalize()}", file=image)
                else:
                    await message.author.send(f"Vous êtes désormais niveau **{level}** !")
            except discord.errors.Forbidden:
                pass

        if self.debug:
            debug_embed.add_field(name="Final Last Message Date", value=f"{profile['last_message_date']}")

        return_code = await db.update_profile(message.author.id, profile)

        if self.debug:
            debug_embed.description += f"DB return code : {return_code}\n\nProfile updated\n```"
            debug_embed.set_footer(icon_url=message.author.avatar_url,
                                   text=f"Message by {message.author} | {message.created_at}")
            debug_embed.colour = 0x000000
            await self.debug_channel.send(embed=debug_embed)

        try:
            if ctx.valid:
                if lib.get_role(message.guild, 251419879951958027) in message.author.roles \
                        or lib.get_role(message.guild, 220310157513457664) in message.author.roles \
                        or "!pinned" in message.content:
                    await self.process_commands(message)
                else:
                    if message.channel.id == 485481154124840992:
                        await self.process_commands(message)
                    else:
                        await message.delete()
                        await message.channel.send(f"{message.author.mention} Vous ne pouvez utiliser de commande ici, "
                                                   f"merci d'aller dans #bots-commands", delete_after=10)
        except AttributeError:
            await self.process_commands(message)

    async def check_temp(self, guild):

        temp_sanctions = await db.get_temp_sanctions()
        _date = datetime.utcnow()

        for sanction in temp_sanctions:
            until = sanction['until'].split("+")[0]
            fmt = "%Y-%m-%dT%H:%M:%S.%f"
            # lib.crop_microseconds(until)
            try:
                until = datetime.strptime(until, fmt)
            except ValueError:
                fmt = "%Y-%m-%dT%H:%M:%S"
                until = datetime.strptime(until, fmt)

            user = await db.get_user_from_tmp_sanction(sanction)
            user_id = user['discord_uuid']
            user = self.get_user(user_id)

            if _date < until:
                continue
            else:
                try:
                    if sanction['sanction'] == "Ban":
                        try:
                            await self.http.unban(user_id, guild.id)
                        except AttributeError as e:
                            traceback.print_exception(type(e), e, e.__traceback__, file=sys.stderr)
                            await db.delete_temp_sanction(sanction['uuid'])
                            sakiut = self.get_user(self.owner_id)
                            await sakiut.send(f"Error encountered while trying to auto unban someone :\n"
                                              f"```py\n{traceback.format_exc()}\n``")
                            continue
                    elif sanction['sanction'] == "Mute (text)":
                        if sanction['channel'] is None:
                            for chan in guild.text_channels:
                                await chan.set_permissions(user, overwrite=None)
                            await user.send("Vous avez été unmute de tous les channels de la Flotte de Corobizar, "
                                            "raison : « Sanction expirée ».")
                        else:
                            channel = self.get_channel(sanction["channel"])
                            await channel.set_permissions(user, overwrite=None)
                            await user.send(f"Vous avez été unmute du channel #{channel.name} de la Flotte de Corobizar"
                                            f", raison : « Sanction expirée ».")
                    elif sanction['sanction'] == "Block XP":
                        self.blocked_list = [x for x in self.blocked_list if x != user.id]
                        db.save_xp_blocked(self.blocked_list)
                        await user.send(f"Vous pouvez à nouveau engranger de l'expérience sur la Flotte de Corobizar,"
                                        f"raison : « Sanction expirée ».")

                    elif sanction['sanction'] == "Loser Rename":
                        user = guild.get_member(user.id)
                        await user.edit(nick=None)
                        await user.remove_roles(lib.get_role(guild, 487642902604939264))
                    else:
                        continue
                except discord.errors.Forbidden:
                    pass

                sct = await db.delete_temp_sanction(sanction['uuid'])
                print(f"Temp Sanction deleted : {sct} - {sanction['sanction']}")

    async def stream_start_check(self, channel):

        client_id = lib.get_twitch_client_id()
        secret_key = lib.get_twitch_secret_key()

        check_result = await twitch.check(client_id, secret_key, self.stream_id)
        # stream = check_result[0]
        self.stream_id = check_result[1]
        ping = check_result[2]
        data = check_result[3]

        if ping:
            stream_embed = discord.Embed()
            stream_embed.title = "STREAM ON !"
            stream_embed.colour = 0x3498db
            stream_embed.description = "Un nouveau stream a été lancé, rejoins-nous vite ! \n" \
                                       "[Clique ici ! Oui, clique !](https://twitch.tv/corobizar/)"
            stream_embed.add_field(name="Titre", value=data["title"], inline=False)
            stream_embed.add_field(name="Jeu", value=twitch.get_game_name(client_id, data["game_id"], check_result[4]))
            stream_embed.add_field(name="Viewers", value=data["viewer_count"])
            stream_embed.set_image(url=data['thumbnail_url'].format(width="1920", height="1080"))
            stream_embed.set_thumbnail(url=twitch.get_thumbnail(client_id, data['user_id'], check_result[4]))

            await channel.send("<@&709158421345337344>", embed=stream_embed)

    async def fourasticot_check(self):
        if datetime.utcnow() >= self.next_fourasticot:
            await fourasticot.start(self, random.choice(self.fourasticot_channels),
                                    random.choice(captcha.get_listing()))
            self.next_fourasticot = datetime.utcnow() + timedelta(minutes=random.randint(59, 250))

    async def coin_check(self):
        today = date.today()
        if self.date == today:
            return
        else:
            self.date = date.today()

        # print("Starting initialisation...")
        # iterator = 0

        profiles_list = await db.get_all_profiles()

        if profiles_list is None:
            return

        for profile in profiles_list:
            coins_limit = get_level_coins()
            coins_limit = coins_limit[profile["level"]]

            if profile["coins"] < coins_limit:
                profile["coins"] += 1
                profile["last_coin"] = datetime.today()
            elif profile["coins"] > coins_limit:
                profile["coins"] = coins_limit
            else:
                # iterator += 1
                # print(f"{iterator}/4693 done")
                continue
            await db.update_profile(profile["discord_uuid"], profile)
        #     iterator += 1
        #     print(f"{iterator}/4693 done")
        #
        # print("Initialisation done!")


token = lib.get_lamastibot_token()
bot = Bot(command_prefix=commands.when_mentioned_or("!"), description="Commandes Lamastibot")
bot.help_command = commands.DefaultHelpCommand(dm_help=True)
bot.run(token)
