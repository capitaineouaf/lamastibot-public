"""
Lamastibot — Launcher

SAKIUT Custom License

Copyright (c) 2018-present Sakiut

Permission is hereby granted, free of charge, to any person to obtain a copy
of this software and associated documentation files (the “Software”). Subject
to the foregoing sentence, you are free to modify this Software and publish
patches to the Software. You agree that Sakiut retain all right, title and interest
in and to all such modifications and/or patches, and all such modifications and/or
patches may only be used, copied, modified, displayed, distributed, or otherwise
exploited with the authorization of the author. Notwithstanding the foregoing,
you may not copy and modify the Software for development and testing purposes,
without requiring a subscription. You agree that Sakiut retain all right,
title and interest in and to all such modifications. You are not granted any
other rights beyond what is expressly stated herein. Subject to the
foregoing, it is forbidden to copy, merge, publish, distribute, sublicense,
and/or sell the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

For all third party components incorporated into the Software, those
components are licensed under the original license provided by the owner of the
applicable component.

contact@sakiut.fr
"""

import subprocess
import sys
import time
from datetime import datetime

interpreter = sys.executable
if interpreter is None:     # This should never happen
    raise RuntimeError("Couldn't find Python's interpreter")


def reload_twins_bots(bot_1, bot_2):
    bot_1.kill()
    bot_2.kill()

    bot_1 = subprocess.Popen(["python", "Lamastibot.py"])
    bot_2 = subprocess.Popen(["python", "Fourasticot.py"])

    return bot_1, bot_2


lamastibot = subprocess.Popen(["python", "Lamastibot.py"])
fourasticot = subprocess.Popen(["python", "Fourasticot.py"])

today = 0

while True:

    act_time = datetime.now()

    if act_time < datetime.now().replace(hour=4, minute=0, second=0, microsecond=0):
        today = 0

    elif act_time < datetime.now().replace(hour=16, minute=0, second=0, microsecond=0):

        if today > 0:
            continue

        lamastibot, fourasticot = reload_twins_bots(lamastibot, fourasticot)
        today = 1

    else:

        if today > 1:
            continue

        lamastibot, fourasticot = reload_twins_bots(lamastibot, fourasticot)
        today = 2
