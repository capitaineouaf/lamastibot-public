"""
Lamastibot — Fourasticot's captchas management lib

SAKIUT Custom License

Copyright (c) 2018-present Sakiut

Permission is hereby granted, free of charge, to any person to obtain a copy
of this software and associated documentation files (the “Software”). Subject
to the foregoing sentence, you are free to modify this Software and publish
patches to the Software. You agree that Sakiut retain all right, title and interest
in and to all such modifications and/or patches, and all such modifications and/or
patches may only be used, copied, modified, displayed, distributed, or otherwise
exploited with the authorization of the author. Notwithstanding the foregoing,
you may not copy and modify the Software for development and testing purposes,
without requiring a subscription. You agree that Sakiut retain all right,
title and interest in and to all such modifications. You are not granted any
other rights beyond what is expressly stated herein. Subject to the
foregoing, it is forbidden to copy, merge, publish, distribute, sublicense,
and/or sell the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

For all third party components incorporated into the Software, those
components are licensed under the original license provided by the owner of the
applicable component.

contact@sakiut.fr
"""

from PIL import Image, ImageDraw, ImageFont
import random
import string


def generate(limit: int = 250):

    cap_list = []

    for x in range(limit):
        cap_list.append(''.join(random.choices(string.ascii_lowercase + string.digits, k=12)))

    for cap in cap_list:
        img = Image.new('RGB', (120, 30), color="white")

        d = ImageDraw.Draw(img)
        font = ImageFont.truetype("arial.ttf", 15)

        d.text((10, 10), cap, font=font, fill=(0, 0, 0))
        img.save(f'./data/captcha_source/captcha_{cap}.png')

    with open("./data/captcha_source/listing.txt", 'w') as f:
        for x in cap_list:
            f.write(f"{x}\n")
        f.close()


def get_listing():
    with open("./data/captcha_source/listing.txt", 'r') as f:
        listing = f.read().splitlines()
        f.close()
    return listing


def get_captcha(captcha_id):
    return f"./data/captcha_source/captcha_{captcha_id}.png"
