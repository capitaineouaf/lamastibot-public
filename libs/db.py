"""
Lamastibot — Database's management lib

SAKIUT Custom License

Copyright (c) 2018-present Sakiut

Permission is hereby granted, free of charge, to any person to obtain a copy
of this software and associated documentation files (the “Software”). Subject
to the foregoing sentence, you are free to modify this Software and publish
patches to the Software. You agree that Sakiut retain all right, title and interest
in and to all such modifications and/or patches, and all such modifications and/or
patches may only be used, copied, modified, displayed, distributed, or otherwise
exploited with the authorization of the author. Notwithstanding the foregoing,
you may not copy and modify the Software for development and testing purposes,
without requiring a subscription. You agree that Sakiut retain all right,
title and interest in and to all such modifications. You are not granted any
other rights beyond what is expressly stated herein. Subject to the
foregoing, it is forbidden to copy, merge, publish, distribute, sublicense,
and/or sell the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

For all third party components incorporated into the Software, those
components are licensed under the original license provided by the owner of the
applicable component.

contact@sakiut.fr
"""

import discord
import aiohttp
import pickle
import uuid
import random

from datetime import datetime, timedelta

from libs.lib import get_db_token as gdt
from libs.lib import get_db_host as gdh


token = gdt()
host = gdh()
header = {'authorization': f'token {token}'}


########################################################################################################################
#                                                       PROFILES                                                       #
########################################################################################################################


async def get_profile(user=None, user_id=None):

    if user is not None:
        user_id = user.id

    async with aiohttp.ClientSession() as session:
        async with session.get(f"{host}/api/profile/{user_id}/", headers=header) as q:
            if q.status == 200:
                result = await q.json()
                return result
            elif q.status == 404:
                return None
            else:
                raise ConnectionError("Cannot join database")


async def create_profile(user):

    data = {
        "name": f"{user.name.encode('ascii', 'ignore').decode()}#{user.discriminator}",
        "discord_uuid": user.id,
        "level": 0,
        "xp_points": 0,
        "date": datetime.now(),
        "last_message_date": user.joined_at,
        "coins": 0,
    }

    async with aiohttp.ClientSession() as session:
        await session.post(f"{host}/api/profile/", headers=header, data=data)

    return await get_profile(user)


async def update_profile(user_id, data):

    async with aiohttp.ClientSession() as session:
        async with session.put(f"{host}/api/profile/{user_id}/", headers=header, data=data) as r:
            return r.status


async def get_all_profiles():

    results = []
    page_nbr = 1

    async with aiohttp.ClientSession() as session:
        while page_nbr != 0:
            async with session.get(f"{host}/api/profile/?page={page_nbr}", headers=header) as q:
                if q.status == 200:
                    result = await q.json()
                else:
                    raise ConnectionError("Cannot join database")

                results += result['results']
                if result['next'] is None:
                    page_nbr = 0
                else:
                    page_nbr += 1

    return results


async def delete_profile(discord_uuid):
    async with aiohttp.ClientSession() as session:
        async with session.delete(f"{host}/api/profile/{discord_uuid}/", headers=header) as r:
            return r.status


async def get_top():
    async with aiohttp.ClientSession() as session:
        async with session.get(f"{host}/api/profile/?ordering=-xp_points,max_level_date", headers=header) as q:

            if q.status == 200:
                result = await q.json()
            else:
                raise ConnectionError("Cannot join database")

            return result["results"]


########################################################################################################################
#                                                 ENTRIES & SANCTIONS                                                  #
########################################################################################################################


async def create_entry(bot, user, sanction, channel, author, date, reason=None, comment=None):
    profile = await get_profile(user)
    if profile is None:
        await create_profile(user)

    comment = "Aucun" if comment is None else comment
    reason = "Non spécifiée" if reason is None else reason

    data = {
        "target": f"{host}/api/profile/{user.id}/",
        "reason": reason,
        "comment": comment,
        "author": author,
        "sanction": sanction,
        "channel": channel,
        "modified_date": date,
        "date": date,
    }

    debug_embed = None

    if bot.debug:
        debug_embed = discord.Embed()
        debug_embed.colour = 0xff0000
        debug_embed.set_thumbnail(url=user.avatar_url)
        debug_embed.title = f"New report entry concerning {user}"
        debug_embed.add_field(name="Sanction", value=sanction)
        debug_embed.add_field(name="Channel", value=channel)
        debug_embed.add_field(name="Author", value=author)
        debug_embed.add_field(name="Reason", value=sanction)
        debug_embed.add_field(name="Comment", value=comment)
        debug_embed.add_field(name="Date", value=date)

    async with aiohttp.ClientSession() as session:
        await session.post(f"{host}/api/report_entry/", headers=header, data=data)

    return debug_embed if bot.debug else 0


async def add_temp_sanction(user, sanction, date, until, channel=None):
    profile = await get_profile(user)
    if profile is None:
        await create_profile(user)

    _uuid = uuid.uuid4()

    data = {
        "target": f"{host}/api/profile/{user.id}/",
        "sanction": sanction,
        "uuid": _uuid,
        "channel": channel,
        "date": date,
        "until": until,
    }

    async with aiohttp.ClientSession() as session:
        await session.post(f"{host}/api/temp_sanction/", headers=header, data=data)

    return _uuid


async def get_temp_sanctions():
    async with aiohttp.ClientSession() as session:
        async with session.get(f"{host}/api/temp_sanction/", headers=header) as q:
            if q.status == 200:
                result = await q.json()
            else:
                raise ConnectionError("Cannot join database")

            results = result['results']
            return results


async def delete_temp_sanction(_uuid):
    async with aiohttp.ClientSession() as session:
        async with session.delete(f"{host}/api/temp_sanction/{_uuid}/", headers=header) as q:
            return q.status


async def get_user_from_tmp_sanction(sanction):
    link = sanction['target']

    async with aiohttp.ClientSession() as session:
        async with session.get(link, headers=header) as q:

            if q.status == 200:
                result = await q.json()
            elif q.status == 404:
                return None
            else:
                raise ConnectionError("Cannot join database")

            return result


async def lose_rename(user, role):
    await user.edit(nick="LOSER")
    await user.add_roles(role)
    until = datetime.utcnow() + timedelta(minutes=10)
    await add_temp_sanction(user, "Loser Rename", datetime.now(), until)


########################################################################################################################
#                                                         XP                                                           #
########################################################################################################################


def get_xp_blocked():
    with open('data/exp_blocked.data', 'rb') as f:
        unpickler = pickle.Unpickler(f)
        data = unpickler.load()
        f.close()
    return data


def save_xp_blocked(data):
    with open('data/exp_blocked.data', 'wb') as f:
        pickler = pickle.Pickler(f)
        pickler.dump(data)
        f.close()


async def add_xp(user, amount):
    profile = await get_profile(user)
    if profile is None:
        profile = await create_profile(user)
    profile["xp_points"] += amount
    await update_profile(user.id, profile)


########################################################################################################################
#                                                     BUG REPORTS                                                      #
########################################################################################################################


async def create_bug_report(author, date, title, content):

    report_id = f"{uuid.uuid4()}"

    data = {
        "author": f"{host}/api/profile/{author.id}/",
        "report_id": report_id,
        "date": date,
        "title": title,
        "content": content,
        "status": "NEW",
    }

    async with aiohttp.ClientSession() as session:
        async with session.post(f"{host}/api/bug_report/", headers=header, data=data) as r:
            if r.status == 201:
                result = await r.json()
                return result["report_id"]
            else:   # This should never happen
                return None


########################################################################################################################
#                                                   SCHEDULED EVENTS                                                   #
########################################################################################################################


async def create_scheduled_event(start_date, end_date, role_id=None):

    title = f"Event #{random.randint(0,1000)}"

    data = {
        "title": title,
        "role_id": role_id,
        "start_date": start_date,
        "end_date": end_date,
    }

    async with aiohttp.ClientSession() as session:
        await session.post(f"{host}/api/scheduled_event/", headers=header, data=data)


########################################################################################################################
#                                                      DB XP BOOST                                                     #
########################################################################################################################


def save_boost(ctx, xp_boost):

    data = {
        "boost": xp_boost,
        "date": ctx.message.created_at,
        "user": ctx.author.id
    }

    with open('data/xp_boost.data', 'wb') as f:
        pickler = pickle.Pickler(f)
        pickler.dump(data)
        f.close()


def get_boost():
    with open('data/xp_boost.data', 'rb') as f:
        unpickler = pickle.Unpickler(f)
        data = unpickler.load()
        f.close()
    return data
