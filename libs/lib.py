"""
Lamastibot — Essential Functions

SAKIUT Custom License

Copyright (c) 2018-present Sakiut

Permission is hereby granted, free of charge, to any person to obtain a copy
of this software and associated documentation files (the “Software”). Subject
to the foregoing sentence, you are free to modify this Software and publish
patches to the Software. You agree that Sakiut retain all right, title and interest
in and to all such modifications and/or patches, and all such modifications and/or
patches may only be used, copied, modified, displayed, distributed, or otherwise
exploited with the authorization of the author. Notwithstanding the foregoing,
you may not copy and modify the Software for development and testing purposes,
without requiring a subscription. You agree that Sakiut retain all right,
title and interest in and to all such modifications. You are not granted any
other rights beyond what is expressly stated herein. Subject to the
foregoing, it is forbidden to copy, merge, publish, distribute, sublicense,
and/or sell the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

For all third party components incorporated into the Software, those
components are licensed under the original license provided by the owner of the
applicable component.

contact@sakiut.fr
"""

import datetime
from datetime import timedelta
import asyncio

from libs.xp_levels import *


########################################################################################################################
#                                          Authenticators' data retrieving                                             #
########################################################################################################################


def get_lamastibot_token():
    with open('./config.txt') as f:
        lines = f.read().splitlines()
        f.close()
    line = list(filter(lambda x: x.startswith('lamastibot_token='), lines))[0].split("=")
    token = line[1]
    return token


def get_fourasticot_token():
    with open('./config.txt') as f:
        lines = f.read().splitlines()
        f.close()
    line = list(filter(lambda x: x.startswith('fourasticot_token='), lines))[0].split("=")
    token = line[1]
    return token


def get_db_token():
    with open('./config.txt') as f:
        lines = f.read().splitlines()
        f.close()
    line = list(filter(lambda x: x.startswith('db_token='), lines))[0].split("=")
    token = line[1]
    return token


def get_db_host():
    with open('./config.txt') as f:
        lines = f.read().splitlines()
        f.close()
    line = list(filter(lambda x: x.startswith('db_host='), lines))[0].split("=")
    host = line[1]
    return host


def get_twitch_client_id():
    with open('./config.txt') as f:
        lines = f.read().splitlines()
        f.close()
    line = list(filter(lambda x: x.startswith('client_id='), lines))[0].split("=")
    client_id = line[1]
    return client_id


def get_twitch_secret_key():
    with open('./config.txt') as f:
        lines = f.read().splitlines()
        f.close()
    line = list(filter(lambda x: x.startswith('secret_key='), lines))[0].split("=")
    secret_key = line[1]
    return secret_key


def get_roles_message_id():
    with open('./config.txt') as f:
        lines = f.read().splitlines()
        f.close()
    line = list(filter(lambda x: x.startswith('roles_msg='), lines))[0].split("=")
    roles_msg = line[1]
    return int(roles_msg)


########################################################################################################################
#                                                    Formatting                                                        #
########################################################################################################################


def format_date_time(date):
    line = []
    for attr in ['year', 'month', 'day', 'hour', 'minute', 'second', 'microsecond']:
        line.append(getattr(date, attr))

    dt = "{2}/{1}/{0} {3}:{4}:{5}".format(line[0], line[1], line[2], line[3], line[4], line[5])
    return dt


def discord_date_formatter(date: str):
    """Convertit le format de lecture d'une date"""
    date = str(date)

    date_2 = date.split('-')
    date_3 = date_2[2]
    date_4 = date_3.split(' ')
    date_5 = date_4[1]
    date_6 = date_5.split('.')
    date_7 = date_6[0]
    # noinspection PyUnusedLocal
    date_8 = date_7.split(':')

    final_date = '{0}/{1}/{2} à {3}'.format(date_4[0], date_2[1], date_2[0], date_6[0])
    return final_date


def datetime_formatter(date: str):
    date2 = date.split("T")
    date3 = date2[0].split('-')
    date4 = date2[1][:8].split(':')

    return datetime.datetime(
        int(date3[0]),
        int(date3[1]),
        int(date3[2]),
        int(date4[0]),
        int(date4[1]),
        int(date4[2])
    )


########################################################################################################################
#                                                Discord operations                                                    #
########################################################################################################################


def get_user_roles(user):
    """Retourne une série de str, les roles de l'utilisateur"""

    roles = user.roles
    if not roles:
        return "N/A"

    role_list = []
    role_str = ""

    for x in range(len(roles)):
        role = roles[x].name
        role_list.append(role)

    del role_list[0]

    for x in range(len(role_list)):
        role = role_list[x]
        role_str += role + ", "

    role_final = role_str.rstrip(', ')

    return role_final


async def delete_level_roles(user):
    level_roles = list(filter(lambda x: x.id in get_level_roles(), user.roles))
    for role in level_roles:
        await user.remove_roles(role)


def get_time_master(guild):
    time_master_role = list(filter(lambda x: x.id == 484795749041831998, guild.roles))[0]
    return time_master_role


def is_time_master(user, guild):
    time_master_role = get_time_master(guild)

    for role in user.roles:
        if role.id == time_master_role.id:
            return True
    return False


def get_everyone_channels():
    return [
        485475102344085515,
        220592688414588928,
        496599107448733697,
        220674548134051841,
        485474546263261184,
        220673302580953090,
        349325482782621698,
    ]


def get_role(guild, role_id):
    role = list(filter(lambda x: x.id == role_id, guild.roles))
    return role[0]


async def delete_color_roles(user):
    color_roles = [
        487642024984707073,
        487642604465422336,
        488010490526826529,
        487641670972866561,
        516260823774134293,
        487637704021114924,
        522139983348367360,
        508285186321022998,
    ]
    level_roles = list(filter(lambda x: x.id in color_roles, user.roles))
    for role in level_roles:
        await user.remove_roles(role)


########################################################################################################################
#                                                 Utility functions                                                    #
########################################################################################################################


async def convert_duration(ctx, duration, default_delta=None, default_comment=None):
    duration_nbr = float(duration[:-1])
    duration_unit = duration[-1]

    if duration_unit not in ('j', 'h', 'm', 's'):
        await ctx.send("L'unité donnée est incorrecte", delete_after=5)
        raise ValueError("L'unité donnée est incorrecte")

    delta = default_delta
    comment = default_comment

    if duration_unit == 'j':
        delta = timedelta(days=duration_nbr)
        comment = f"{duration_nbr} jour(s)"
    elif duration_unit == 'h':
        delta = timedelta(hours=duration_nbr)
        comment = f"{duration_nbr} heure(s)"
    elif duration_unit == 'm':
        delta = timedelta(minutes=duration_nbr)
        comment = f"{duration_nbr} minute(s)"
    elif duration_unit == "s":
        delta = timedelta(seconds=duration_nbr)
        comment = f"{duration_nbr} seconde(s)"

    return delta, comment


def crop_microseconds(delta):
    return delta - datetime.timedelta(microseconds=delta.microsecond)
