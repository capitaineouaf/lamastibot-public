"""
Lamastibot — Twitch's API Library

SAKIUT Custom License

Copyright (c) 2018-present Sakiut

Permission is hereby granted, free of charge, to any person to obtain a copy
of this software and associated documentation files (the “Software”). Subject
to the foregoing sentence, you are free to modify this Software and publish
patches to the Software. You agree that Sakiut retain all right, title and interest
in and to all such modifications and/or patches, and all such modifications and/or
patches may only be used, copied, modified, displayed, distributed, or otherwise
exploited with the authorization of the author. Notwithstanding the foregoing,
you may not copy and modify the Software for development and testing purposes,
without requiring a subscription. You agree that Sakiut retain all right,
title and interest in and to all such modifications. You are not granted any
other rights beyond what is expressly stated herein. Subject to the
foregoing, it is forbidden to copy, merge, publish, distribute, sublicense,
and/or sell the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

For all third party components incorporated into the Software, those
components are licensed under the original license provided by the owner of the
applicable component.

contact@sakiut.fr
"""

import requests
import aiohttp


async def check(client_id, secret_key, original_stream_id):

    stream = False
    stream_id = original_stream_id
    ping = False
    data = None

    parameters = {
        "client_id": client_id,
        "client_secret": secret_key,
        "grant_type": "client_credentials"
    }

    async with aiohttp.ClientSession() as session:
        async with session.post("https://id.twitch.tv/oauth2/token", params=parameters) as r:
            if r.status == 200:
                req = await r.json()
            else:
                raise ConnectionError("Cannot connect to database")

    access_token = req["access_token"]

    header = {
        'Authorization': 'Bearer ' + access_token,
        'client-id': client_id,
    }

    async with aiohttp.ClientSession() as session:
        async with session.get(f"https://api.twitch.tv/helix/streams?user_login=corobizar", headers=header) as r:
            if r.status == 200:
                req = await r.json()
            elif r.status == 401:
                raise ConnectionRefusedError("Error 401: Unauthorized")
            else:
                raise ConnectionError(f"Error {req.status} — Can't connect to Twitch DB")

    if len(req["data"]) > 0:
        stream = True
        stream_id = req["data"][0]["id"]
        if stream_id != original_stream_id:
            ping = True
        data = req["data"][0]

    return stream, stream_id, ping, data, access_token


async def get_thumbnail(client_id, user_id, access_token):

    header = {
        'Authorization': 'Bearer ' + access_token,
        'client-id': client_id,
    }

    async with aiohttp.ClientSession() as session:
        async with session.get(f"https://api.twitch.tv/helix/users?id={user_id}", headers=header) as r:
            if r.status == 200:
                req = await r.json()
                thumb_link = req["data"][0]["profile_image_url"]
                return thumb_link
            elif r.status == 401:
                raise ConnectionRefusedError("Error 401: Unauthorized")
            else:
                raise ConnectionError(f"Error {r.status} — Can't connect to Twitch DB")


async def get_game_name(client_id, game_id, access_token):
    header = {
        'Authorization': 'Bearer ' + access_token,
        'client-id': client_id,
    }

    async with aiohttp.ClientSession() as session:
        async with session.get(f"https://api.twitch.tv/helix/games?id={game_id}", headers=header) as r:
            if r.status == 200:
                req = await r.json()
                game_name = req["data"][0]["name"]
                return game_name
            elif r.status == 401:
                raise ConnectionRefusedError("Error 401: Unauthorized")
            else:
                raise ConnectionError(f"Error {r.status} — Can't connect to Twitch DB")
